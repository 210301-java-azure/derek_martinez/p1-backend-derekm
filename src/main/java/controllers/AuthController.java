package controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.AuthService;
import services.UserService;

public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserService userService = new UserService();
    private AuthService authService = new AuthService();
    private String username;
    private String password;

    public void authenticateLogin(Context context){

        if (context.formParam("username")==null || context.formParam("password")==null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");

        username = context.formParam("username");
        password = context.formParam("password");


        logger.info(username + " is attempting to login");
        boolean usernameExists = authService.checkUsername(username);

        if(!usernameExists){
            context.status(400);
            logger.error(username + " does not exist in db");
            return;
        }

        boolean isPasswordCorrect = authService.checkPassword(username, password);

        if(!isPasswordCorrect){
            context.status(400);
            context.result("check your credentials and try again.");
            logger.error("password is incorrect.");
            return;
        }

        User user = userService.getUserByUsername(username);

        if(user.isAdmin()){
            context.header("Authorization","admin-auth-token");
            context.header("User-Id", String.valueOf(user.getUserId()));

            context.result("credentials successfully verified.");
            logger.info("admin: " + username + "'s credentials have been successfully verified");
            context.status(200);
        } else{
            context.header("Authorization","user-auth-token");
            context.header("User-Id", String.valueOf(user.getUserId()));
            context.result("credentials successfully verified.");
            logger.info("user: " + username + "'s credentials have been successfully verified");
            context.status(200);
        }
    }

    public void authorizeToken(Context context){
        logger.info("attempting to authorize token");

        String authHeader = context.header("Authorization");

        if(authHeader != null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("not authorized");
            throw new UnauthorizedResponse();
        }
    }


}
