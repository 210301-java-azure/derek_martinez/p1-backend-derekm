package controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.http.UnauthorizedResponse;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.AuthService;
import services.UserService;


public class UserController {

    private String username;
    private String password;
    private String email;
    private boolean isAdmin;

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService = new UserService();
    private final AuthService authService = new AuthService();

    public void handleGetAllUsersRequest(Context context){

        if(authService.hasAdminAccess(context)){
            logger.info("getting all users");
            context.json(userService.getAll());
        } else{
            logger.warn("not authorized");
            throw new UnauthorizedResponse();
        }

    }

    public void handleGetUserByIdRequest(Context context){

        String targetUserId = context.pathParam("id");
        String headerUserId = context.header("User-Id");

        if( authService.hasAdminAccess(context) || (targetUserId.equals(headerUserId))){

            if(targetUserId.matches("^\\d+$")){

                int userIdInput = Integer.parseInt(targetUserId);
                User user = userService.getById(userIdInput);
                logger.info("retrieving user information for: " + user.getUsername());

                if(user == null){
                    logger.warn("no user found with id: " + targetUserId);
                    throw new NotFoundResponse("No user found with provided id:" + targetUserId);
                } else{
                    context.json(user);
                }
            } else{
                throw new BadRequestResponse("id input: \"" + targetUserId + "\"cannot be parsed to an int");
            }

        } else{
            logger.warn("not authorized");
            throw new UnauthorizedResponse();
        }



    }

/*    public void handleCreateUserRequest(Context context){

        User user = context.bodyAsClass(User.class);

        if(authService.hasAdminAccess(context)){
            logger.info("attempting to add new user: " + user);
            userService.add(user);
            context.status(201);
        }

    }*/

    public void handleDeleteUserByIdRequest(Context context){

        String userIdString = context.pathParam("id");

        if(userIdString.matches("^\\d+$")){
            if(authService.hasAdminAccess(context)){
                int userIdInput = Integer.parseInt(userIdString);
                logger.info("deleting post with id: " + userIdInput);
                userService.delete(userIdInput);
            } else{
                throw new UnauthorizedResponse("you are not authorized to delete users");
            }
        } else{
            throw new BadRequestResponse("input \""+userIdString+"\" cannot be parsed to an int");
        }


    }

    public void handleUpdateUserByIdRequest(Context context) {

        String targetUserId = context.pathParam("id");
        String headerUserId = context.header("User-Id");

        User user = context.bodyAsClass(User.class);

        if(targetUserId.matches("^\\d+$")){
            if(authService.hasAdminAccess(context) || (targetUserId.equals(headerUserId))){
                int userId = Integer.parseInt(targetUserId);
                logger.info("attempting info update for user: " + userId);
                userService.update(userId,user);
            } else{
                throw new UnauthorizedResponse();
            }
        } else{
            throw new BadRequestResponse();
        }
    }

    public void handleGetPostsByUserIdRequest(Context context) {
        String userIdString = context.pathParam("id");

        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            logger.info("retrieving all posts authored by userid:" + userId);
            context.json(userService.getPosts(userId));
        }
    }

    public void handleRegisterNewUserRequest(Context context) {


        if (context.formParam("username") == null || context.formParam("password") == null || context.formParam("email") == null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");

        username = context.formParam("username");
        password = context.formParam("password");
        email = context.formParam("email");
        isAdmin = false;


        logger.info("attempting creation of user: " + username);


        int result = userService.registerNewUser(username, password, email, isAdmin);

        switch (result) {
            case 0:
                throw new BadRequestResponse("user account not created.");
            case 1:
                context.result("User account with username: " + username + " created.");
                logger.info("user: " + username + ", successfully created.");
                break;
            case 2:
                throw new BadRequestResponse("account with username: " + username + " already exists.");
            case 3:
                throw new BadRequestResponse("account with email: " + email + " already exists.");

        }

    }

}
