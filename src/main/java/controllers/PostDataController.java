package controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.http.UnauthorizedResponse;
import models.Comment;
import models.Post;
import models.PseudoComment;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.AuthService;
import services.PostService;
import services.UserService;

import java.time.LocalDate;
import java.sql.Date;


public class PostDataController {

    private Logger logger = LoggerFactory.getLogger(PostDataController.class);
    private PostService postService = new PostService();
    private UserService userService = new UserService();
    private AuthService authService = new AuthService();


    public void handleGetPostsRequest(Context context) {
        logger.info("getting all posts");
        context.json(postService.getAll());
    }

    public void handleGetPostByIdRequest(Context context) {
        String postIdString = context.pathParam("id");

        if(postIdString.matches("^\\d+$")){
            int postIdInput = Integer.parseInt(postIdString);

            Post post = postService.getById(postIdInput);

            if(post == null){
                logger.warn("no post present with id: " + postIdInput);
                throw new NotFoundResponse("No post found with provided ID: " + postIdInput);
            } else {
                logger.info("getting post with id: " + postIdInput);
                context.json(post);
            }
        } else {
            throw new BadRequestResponse("id input: \"" + postIdString + "\"cannot be parsed to an int");
        }
    }

    public void handleCreatePostRequest (Context context){

        if (context.header("Authorization")==null || context.header("User-Id")==null) {
            logger.error("post could not be created");
            throw new UnauthorizedResponse("token and userid required to create post");
        }

        if(authService.hasAdminAccess(context) || authService.hasUserAccess(context)){
            Post post = context.bodyAsClass(Post.class);
            int headerUserId = Integer.parseInt(context.header("User-Id"));
            User user = userService.getById(headerUserId);
            logger.info("user: " + user.getUsername() + " is attempting to create a new post.");
            post.setUser(user);
            post.setDateCreated(Date.valueOf(LocalDate.now()));
            postService.add(post);
            logger.info("post created successfully");
            context.status(201);
        }

    }

    public void handleUpdatePostByIdRequest(Context context){
        String postIdString = context.pathParam("id");
        String headerUserId = context.header("User-Id");

        if(postIdString.matches("^\\d+$")){

            int postIdInput = Integer.parseInt(postIdString);
            int userId = Integer.parseInt(headerUserId);

            Post post = postService.getById(postIdInput);
            User user = post.getUser();

            if(user.getUserId() != userId){
                throw new UnauthorizedResponse("you can only edit your own posts.");
            } else{
                Post updatedPost = context.bodyAsClass(Post.class);
                updatedPost.setPostId(post.getPostId());
                updatedPost.setUser(user);
                updatedPost.setDateCreated(post.getDateCreated());
                postService.update(updatedPost);
            }
        }
    }

    public void handleDeletePostByIdRequest(Context context){

        String postIdString = context.pathParam("id");
        String headerUserId = context.header("User-Id");

        if(postIdString.matches("^\\d+$")){

            int postId = Integer.parseInt(postIdString);
            int userId = Integer.parseInt(headerUserId);

            Post post = postService.getById(postId);
            User user = post.getUser();

            if(authService.hasAdminAccess(context) || (userId == user.getUserId())){
                int postIdInput = Integer.parseInt(postIdString);
                logger.info("deleting post and all comments associated with with post-id: " + postIdInput);
                postService.delete(postIdInput);
            } else{
                throw new UnauthorizedResponse("you are not authorized to delete this post");
            }
        } else{
            throw new BadRequestResponse("input \""+postIdString+"\" cannot be parsed to an int");
        }



    }


    public void handleCreateNewCommentRequest(Context context) {

        if (context.header("Authorization")==null || context.header("User-Id")==null) {
            logger.error("comment could not be created");
            throw new UnauthorizedResponse("token and userid required to create post");
        }

        if(authService.hasAdminAccess(context) || authService.hasUserAccess(context)){

            PseudoComment tempComment = context.bodyAsClass(PseudoComment.class);
            Post post = postService.getById(tempComment.getPostId());
            String commentBody = tempComment.getCommentBody();
            int headerUserId = Integer.parseInt(context.header("User-Id"));
            User user = userService.getById(headerUserId);

            if(user == null || post == null){
                throw new BadRequestResponse();
            } else{
                logger.info("user: " + user.getUsername() + " is attempting to create a new comment");
                Comment comment = new Comment(post, user, commentBody);

                comment.setDateCreated(Date.valueOf(LocalDate.now()));
                postService.addComment(comment);
                logger.info("comment created successfully");
                context.status(201);
            }

        }

    }

    public void handleGetCommentsByPostIdRequest(Context context) {
        String postIdString = context.pathParam("id");

        if(postIdString.matches("^\\d+$")){
            int postId = Integer.parseInt(postIdString);
            logger.info("retrieving all comments from postid:" + postIdString);
            context.json(postService.getComments(postId));
        }
    }
}
