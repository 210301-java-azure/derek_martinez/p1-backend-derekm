package models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "posts")
public class Post implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int postId;
    private String title;

    @ManyToOne
    private User user;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateCreated;
    private String postBody;

    public Post(){
        super();
    }

    public Post(int postId, String title, User user, Date dateCreated, String postBody) {
        this.postId = postId;
        this.title = title;
        this.user = user;
        this.dateCreated = dateCreated;
        this.postBody = postBody;
    }

    public Post(int postId) {
        this.postId = postId;
    }

    public Post(int postId, String title, User testUser, String postBody) {
        this.postId = postId;
        this.title = title;
        this.user = testUser;
        this.postBody = postBody;
    }


 /*   public void addComment(int id, User user, Date date, String message){
        Comment newComment = new Comment(this.postId, id, user,date,message);
        commentList.add(newComment);
    }*/

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
