import controllers.AuthController;
import controllers.PostDataController;
import controllers.UserController;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import util.SecurityUtil;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.get;

public class JavalinApp {


    PostDataController postDataController = new PostDataController();
    AuthController authController = new AuthController();
    UserController userController = new UserController();
    SecurityUtil securityUtil = new SecurityUtil();


    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {

        path("/posts", () -> {
            get(postDataController::handleGetPostsRequest);
            post(postDataController::handleCreatePostRequest);
            path("/:id", () -> {
                get(postDataController::handleGetPostByIdRequest);
                put(postDataController::handleUpdatePostByIdRequest);
                delete(postDataController::handleDeletePostByIdRequest);
                path("/comments", () -> {
                    get(postDataController::handleGetCommentsByPostIdRequest);
                });
            });


        });

        path("/login", () -> {
            post(authController::authenticateLogin);
            after("/", securityUtil::attachResponseHeaders);
        });

        path("/register", () -> {
            post(userController::handleRegisterNewUserRequest);
        });

        path("/users", () -> {
            get(userController::handleGetAllUsersRequest);
            //post(userController::handleRegisterNewUserRequest);
            path("/:id", () -> {
                get(userController::handleGetUserByIdRequest);
                put(userController::handleUpdateUserByIdRequest);
                delete(userController::handleDeleteUserByIdRequest);

                path("/posts", () -> {
                    get(userController::handleGetPostsByUserIdRequest);
                });
            });
        });

        path("/comments", () -> {
            post(postDataController::handleCreateNewCommentRequest);
        });

    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
