package data;

import models.Comment;
import models.Post;
import models.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class PostDaoHibImpl implements PostDao{

    @Override
    public List<Post> getAllPosts() {
        try(Session session = HibernateUtil.getSession()){
            List<Post> posts = session.createQuery("from Post", Post.class).list();
            return posts;
        }
    }

    @Override
    public Post getPostById(int postId) {
        try(Session session = HibernateUtil.getSession()){
            Post post = session.get(Post.class, postId);
            return post;
        }
    }

    @Override
    public void addNewPost(Post post) {

        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            session.save(post);
            transaction.commit();
        }

    }

    @Override
    public void deletePost(int postId) {

        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();

            Query q = session.createQuery("delete Comment where post = " + postId);
            q.executeUpdate();


            session.delete(new Post(postId));
            transaction.commit();

        }

    }

    @Override
    public void updatePost(Post post) {
        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaUpdate<Post> criteria = builder.createCriteriaUpdate(Post.class);
            Root<Post> root = criteria.from(Post.class);
            criteria.set("title", post.getTitle());
            criteria.set("postBody", post.getPostBody());
            criteria.where(builder.equal(root.get("postId"), post.getPostId()));
            session.createQuery(criteria).executeUpdate();
            transaction.commit();
        }


    }

    @Override
    public void addNewComment(Comment comment) {
        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            session.save(comment);
            transaction.commit();
        }
    }

    @Override
    public List<Comment> getCommentsByPostId(int postId) {
        try(Session session = HibernateUtil.getSession()){
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Comment> cq = builder.createQuery(Comment.class);
            Root<Comment> root = cq.from(Comment.class);
            cq.select(root).where(builder.equal(root.get("post"), postId));

            Query<Comment> query = session.createQuery(cq);
            List<Comment> results = query.getResultList();

            return results;
        }

    }
}
