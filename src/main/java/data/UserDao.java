package data;

import models.Post;
import models.User;

import java.util.List;

public interface UserDao {
    
    List<String> getAllUsernames();
    List<String> getAllEmails();

    List<User> getAllUsers();
    User getUserById(int userId);

    void deleteUser(int userId);
    void updateUser(int userId, User user);
    List<Post> getUserPostsByUserId(int userId);

    boolean addNewUser(String username, String password, String email, boolean isAdmin);
    /*    void addNewUser(User user);*/

    boolean findUserInDbByUsername(String user);


    String getUserPassword(String username, String password);


    User getUserByUsername(String username);
}
