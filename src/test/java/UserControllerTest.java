import controllers.PostDataController;
import controllers.UserController;
import io.javalin.http.Context;
import models.Post;
import models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import services.PostService;
import services.UserService;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @InjectMocks
    private PostDataController postController;

    @Mock
    private UserService service;

    @Mock
    private PostService postService;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllPostsHandler(){
        Context context = mock(Context.class);
        User testUser = new User(20,"username1","email1");
        Post testPost = new Post(1, "test title", testUser, "test post body");
        List<Post> posts = new ArrayList<>();
        posts.add(testPost);

        when(postService.getAll()).thenReturn(posts);
        postController.handleGetPostsRequest(context);
        verify(context).json(posts);

    }

}
