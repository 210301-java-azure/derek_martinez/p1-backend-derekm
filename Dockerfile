FROM java:8
COPY build/libs/project-free-society-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar project-free-society-1.0-SNAPSHOT.jar